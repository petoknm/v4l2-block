extern crate v4l2_sys;
extern crate libc;
extern crate serde;
extern crate serde_json;

#[allow(non_upper_case_globals)]
mod sys;

use crate::sys::*;

use std::env;
use std::fmt::{Display, Formatter, Result as FmtResult};
use std::io::BufRead;

use serde::{Serialize, Deserialize};
use serde_json::{to_string as to_json, from_str as from_json};

#[derive(Debug, Deserialize)]
struct Event {
    button: i32,
}

#[derive(Debug, Serialize)]
struct Output {
    full_text: String,
}

#[derive(Debug)]
struct State<'a> {
    dev: &'a Device,
    control_idx: usize,
    controls: Vec<Control<'a>>,
}

impl<'a> Display for State<'a> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let control = &self.controls[self.control_idx];
        let value = control.value_to_string().unwrap();
        write!(f, "{}: {}", control.name, value)
    }
}

fn handle_event(mut state: State, e: Event) -> State {
    let len = state.controls.len();
    let control = &state.controls[state.control_idx];
    match e.button {
        1 /* left */ => state.control_idx += 1,
        3 /* right */ => state.control_idx += len - 1,
        4 /* wheel up */ => { let _ = control.inc(); },
        5 /* wheel down */ => { let _ = control.dec(); },
        _ => {},
    }
    state.control_idx %= len;
    state
}

fn main() {
    let dev_name = env::var("instance").unwrap_or(String::from("/dev/video0"));
    let dev = &Device::open(&dev_name).unwrap();
    let controls = dev.controls().collect();

    let stdin = std::io::stdin();
    let handle = stdin.lock();
    let mut state = State { dev, controls, control_idx: 0 };

    for line in handle.lines() {
        let line = line.unwrap();
        let event = from_json(&line).unwrap();
        let new_state = handle_event(state, event);
        state = new_state;
        let output = Output { full_text: state.to_string() };
        let output = to_json(&output).unwrap();
        println!("{}", output);
    }
}
