const VIDIOC_QUERYCTRL: u64 = 0xc0445624;
const VIDIOC_QUERYMENU: u64 = 0xc02c5625;
const VIDIOC_G_CTRL: u64    = 0xc008561b;
const VIDIOC_S_CTRL: u64    = 0xc008561c;

use std::ffi::{CString, CStr};
use libc::{c_int, open, close, ioctl, O_RDWR};
use v4l2_sys::*;

#[derive(Clone, Debug)]
pub struct Device {
    fd: c_int
}

impl Device {
    pub fn open(device: &str) -> Option<Self> {
        let device = CString::new(device).ok()?;
        let fd = unsafe { open(device.as_ptr(), O_RDWR) };
        if fd < 0 { None } else { Some(Self{fd}) }
    }

    pub fn controls(&self) -> Controls {
        let qctrl = unsafe { std::mem::zeroed() };
        Controls { dev: self, qctrl }
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe { close(self.fd) };
    }
}

#[derive(Clone, Debug)]
pub struct Control<'a> {
    pub id: u32,
    pub name: String,
    inner: ControlInner,
    dev: &'a Device,
}

impl<'a> Control<'a> {
    pub fn value_to_string(&self) -> Result<String, ()> {
        let v = get_ctrl_value_raw(self.dev.fd, self.id)?;
        use ControlInner::*;
        Ok(match self.inner {
            Boolean => format!("{}", v != 0),
            Menu{ref options} => format!("{}", options[v as usize]),
            _ => format!("{}", v),
        })
    }

    pub fn inc(&self) -> Result<(), ()> {
        let val = get_ctrl_value_raw(self.dev.fd, self.id)?;
        use ControlInner::*;
        let new_val = match self.inner {
            Integer{min, max, step} => (val + step).min(max).max(min),
            Menu{ref options} => (val + 1).min(options.len() as i32 - 1).max(0),
            Boolean => val ^ 1,
        };
        set_ctrl_value_raw(self.dev.fd, self.id, new_val)
    }

    pub fn dec(&self) -> Result<(), ()> {
        let val = get_ctrl_value_raw(self.dev.fd, self.id)?;
        use ControlInner::*;
        let new_val = match self.inner {
            Integer{min, max, step} => (val - step).min(max).max(min),
            Menu{ref options} => (val - 1).min(options.len() as i32 - 1).max(0),
            Boolean => val ^ 1,
        };
        set_ctrl_value_raw(self.dev.fd, self.id, new_val)
    }
}

impl<'a> From<(&'a Device, v4l2_queryctrl)> for Control<'a> {
    fn from((dev, qctrl): (&'a Device, v4l2_queryctrl)) -> Control<'a> {
        let id = qctrl.id;
        let name = c_string_to_owned(&qctrl.name);
        let min = qctrl.minimum;
        let max = qctrl.maximum;
        let step = qctrl.step;

        use ControlInner::*;
        let inner = match qctrl.type_ {
            v4l2_ctrl_type_V4L2_CTRL_TYPE_INTEGER => Integer { min, max, step },
            v4l2_ctrl_type_V4L2_CTRL_TYPE_BOOLEAN => Boolean,
            v4l2_ctrl_type_V4L2_CTRL_TYPE_MENU    => Menu {
                options: get_ctrl_menu_options(dev.fd, id, min, max),
            },
            x => panic!("Unknown control type {}", x),
        };

        Control { id, name, dev, inner }
    }
}

fn c_string_to_owned(array: &[u8]) -> String {
    let name = unsafe { CStr::from_ptr(std::mem::transmute(array.as_ptr()))};
    name.to_str().unwrap().to_string()
}

fn get_ctrl_value_raw(fd: c_int, id: u32) -> Result<i32, ()> {
    let mut ctrl = v4l2_control { id, value: 0 };
    let res = unsafe { ioctl(fd, VIDIOC_G_CTRL, &mut ctrl as *mut _) };
    if res == 0 { Ok(ctrl.value) } else { Err(()) }
}

fn set_ctrl_value_raw(fd: c_int, id: u32, value: i32) -> Result<(), ()> {
    let ctrl = v4l2_control { id, value };
    let res = unsafe { ioctl(fd, VIDIOC_S_CTRL, &ctrl as *const _) };
    if res == 0 { Ok(()) } else { Err(()) }
}

fn get_ctrl_menu_options(fd: c_int, id: u32, min: i32, max: i32) -> Vec<String> {
    (min..max+1)
        .map(|index| {
            let mut menu: v4l2_querymenu = unsafe { std::mem::zeroed() };
            menu.id = id;
            menu.index = index as u32;
            let res = unsafe { ioctl(fd, VIDIOC_QUERYMENU, &mut menu as *mut _) };
            // v4l2ucp handles errors by displaying "Unknown"
            // assert_eq!(res, 0);
            if res == 0 {
                c_string_to_owned(unsafe { &menu.__bindgen_anon_1.name })
            } else {
                String::from("Unknown")
            }
        })
        .collect()
}

#[derive(Clone, Debug)]
pub enum ControlInner {
    Integer{ min: i32, max: i32, step: i32 },
    Boolean,
    Menu{ options: Vec<String> },
}

pub struct Controls<'a> {
    dev: &'a Device,
    qctrl: v4l2_queryctrl
}

impl<'a> Iterator for Controls<'a> {
    type Item = Control<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.qctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
        let res = unsafe { ioctl(self.dev.fd, VIDIOC_QUERYCTRL, &mut self.qctrl as *mut _) };
        if res != 0 {
            return None;
        }

        let control = (self.dev, self.qctrl).into();
        Some(control)
    }
}
